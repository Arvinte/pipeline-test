#!/bin/bash
for d in */; do
    cd "$d"
    input="Chart.yaml"
    name_prefix="name: "
    version_prefix="version: "
    indent_name_prefix="  - name: "
    indent_version_prefix="    version: "
    while IFS= read -r line; do
        if [[ $line == *"name"* ]]; then
            line=${line#"$name_prefix"}
            line=${line#"$indent_name_prefix"}
            echo "$line"
        fi
        if [[ $line == *"version"* ]]; then
            line=${line#"$version_prefix"}
            line=${line#"$indent_version_prefix"}
            echo "$line"
            echo
        fi
    done <"$input"
    cd ..
done
